Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    el.clickOutsideEvent = function (event) {
      // here I check that click was outside the el and his childrens
      if (!(el == event.target || el.contains(event.target))) {
        // and if it did, call method provided in attribute value
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  },
});


new Vue({
  el: '#app',
  data() {
    return {
      dropmenuActive: false,
      scrollPos: 0,

    //  payment page
      activeTab: 'delivery'
    }
  },
  mounted() {
    this.scrollPos = this.getScrollPos();
    this.checkScroll()
  },
  methods: {
    closeMenu(event) {
      this.dropmenuActive = false;
    },
    dropmenuToggle() {
      this.dropmenuActive = !this.dropmenuActive;
    },
    checkScroll() {
      window.addEventListener('scroll', e => {
        this.scrollPos = this.getScrollPos();
      });
    },
    getScrollPos() {
      return window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    },

    openTab(tab) {
      this.activeTab = tab;
    }
  }
});